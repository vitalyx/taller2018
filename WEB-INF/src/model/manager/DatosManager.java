
package model.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import model.domain.Datos;
import model.domain.Usuarios;

//@Service indica que la clase es un bean de la capa de negocio
@Service
public class DatosManager {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public void setDataSource(DataSource dataSource){
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	public Datos validarLogin(String xlogin){
		BeanPropertyRowMapper bprm = new BeanPropertyRowMapper(Datos.class);
		String xsql="select *  "
		+"  from datos "
		+" where login=?";
		Datos aux=null;
		try {
			aux= (Datos) this.jdbcTemplate.queryForObject(xsql,new Object[] {xlogin},bprm);
		}
		catch(Exception e)
		{
			aux=null;
		}
		return aux;
	}
	public Usuarios extraerDatos(int xidusu){
		BeanPropertyRowMapper bprm = new BeanPropertyRowMapper(Usuarios.class);
		String xsql="select *"
		+"  from usuarios "
		+" where idusu=?";
		Usuarios aux=null;
		try {
			aux= (Usuarios) this.jdbcTemplate.queryForObject(xsql,new Object[] {xidusu},bprm);
		}
		catch(Exception e)
		{
			
			aux=null;
		}
		return aux;
	}
public int insertUsuarios(String nombre,String apellido1,String apellido2, String sexo, Date f_nac,String cedula, String telefono, String direccion, String foto){
		
		String xsql=" insert into usuarios(nombre,apellido1,apellido2,sexo,f_nac,cedula,telefono,direccion,foto) "
				+"   values(?,?,?,?,?,?,?,?,?) ";
		return this.jdbcTemplate.update(xsql, new Object[] {nombre,apellido1,apellido2,sexo,f_nac,cedula,telefono,direccion,foto});
	}
public int insertCarreras(String nombre, String direccion,String telefono){
	
	String xsql=" insert into carreras(nombre,direccion,telefono) "
			+"   values(?,?,?) ";
	return this.jdbcTemplate.update(xsql, new Object[] {nombre,direccion,telefono});
}
public int modUsuarios(int idusu,String nombre,String apellido1,String apellido2, String sexo, Date f_nac,String cedula, String telefono, String direccion, String foto){
	String xsql="  update usuarios "
				+" set nombre=?, apellido1=?, apellido2=?,sexo=?, f_nac=?, cedula=?, telefono=?, direccion=?, foto=?"
				+" where idusu=?";
				
	return this.jdbcTemplate.update(xsql, new Object[] {nombre,apellido1,apellido2,sexo,f_nac,cedula,telefono,direccion,foto,idusu});
}
public int modCarreras(int idcarr,String nombre, String direccion,String telefono){
	String xsql="  update carreras "
				+" set nombre=?, direccion=?, telefono=?"
				+" where idcarr=?";
				
	return this.jdbcTemplate.update(xsql, new Object[] {nombre,direccion,telefono,idcarr});
}
public int insertDatos(int idusu,String login,String password){
	
	String xsql=" insert into datos(idusu,login,password) "
			+"   values(?,?,?) ";
	return this.jdbcTemplate.update(xsql, new Object[] {idusu,login,password});
	/*+"values("+ nombre +","+ apellido1 +","+ apellido2 + ","+ sexo +","+","+ f_nac +","+","+ cedula +","+ telefono +","+ direccion +","+"#"+")";		
	return this.jdbcTemplate.update(xsql);*/
}

public int registrarInscripcion(int idusu,int idcarr, String sigla,String gestion, String paralelo, String periodo){
	try {
		
	
	String xsql=" insert into programacion(idusu,idcarr,sigla,gestion,paralelo,periodo) "
			+"   values(?,?,?,?,?,?) ";
	return this.jdbcTemplate.update(xsql, new Object[] {idusu,idcarr,sigla,gestion,paralelo,periodo});
	} catch (Exception e) {
		// TODO: handle exception
		System.out.println(e.getMessage());
		return -1;
	}	
}

public int deleteInscripcion(int idusu,int idcarr, String sigla,String gestion, String paralelo, String periodo){
	
	String xsql=" delete from programacion"
			+" where idusu=? and idcarr=? and  sigla=? and gestion=? and  paralelo=? and periodo=?";
	return this.jdbcTemplate.update(xsql, new Object[] {idusu,idcarr,sigla,gestion,paralelo,periodo});
}

public int modDatos(int idusu,String login,String password){
	String xsql="  update datos "
				+" set login=?, password=?"
				+" where idusu=?";
	return this.jdbcTemplate.update(xsql, new Object[] {login,password,idusu});
}

public int modDatosLogin(int idusu,String login){
	String xsql="  update datos "
				+" set login=?"
				+" where idusu=?";
	return this.jdbcTemplate.update(xsql, new Object[] {login,idusu});
}
	public List<Map<String,Object>> listarMenus(int xidrol){
		String xsql="select m.idmenu,m.nombre,m.activo "
				+"  from menus m, rolme r "
				+"  where r.idmenu=m.idmenu and r.idrol=? ";		
							
		return this.jdbcTemplate.queryForList(xsql, new Object[] {xidrol});
	}
	public int delUsuarios(int xidusu){		
		String xsql="  update usuarios  "
					+" set activo= false"		
					+" where idusu=? ";
		return this.jdbcTemplate.update(xsql, new Object[] {xidusu});
	}
	public int habUsuarios(int xidusu){		
		String xsql="  update usuarios  "
					+" set activo= true"		
					+" where idusu=? ";
		return this.jdbcTemplate.update(xsql, new Object[] {xidusu});
	}
	public int delCarreras(int xidcarr){		
		String xsql="  update carreras  "
					+" set activo= false"		
					+" where idcarr=? ";
		return this.jdbcTemplate.update(xsql, new Object[] {xidcarr});
	}
	public int habCarreras(int xidcarr){		
		String xsql="  update carreras  "
					+" set activo= true"		
					+" where idcarr=? ";
		return this.jdbcTemplate.update(xsql, new Object[] {xidcarr});
	}
	
	public List<Map<String,Object>> listarSubmenu(){
		String xsql="select m.idmenu,p.nombre,p.enlace,p.descripcion "
		+"  from procesos p, mepro m "
		+"  where m.idpro=p.idpro";		
					
		return this.jdbcTemplate.queryForList(xsql);
	}
	public List<Map<String,Object>> listarRolesUsu(int IDUSU){
		String xsql="SELECT a.idrol, a.idusu, b.idrol, b.nombre, c.nombre " + 
				"FROM usurol a, roles b, usuarios c " + 
				"WHERE a.idrol = b.idrol and a.idusu=c.idusu";	
					
		return this.jdbcTemplate.queryForList(xsql);
	}
	public List<Map<String,Object>> listarUsuarios(){
		String xsql="select * "
		+"  from usuarios "
		+" ";				
		return this.jdbcTemplate.queryForList(xsql);
	}
	public List<Map<String,Object>> listarCarreras(){
		String xsql="select * "
		+"  from carreras "
		+" ";				
		return this.jdbcTemplate.queryForList(xsql);
	}
	public List<Map<String,Object>> listarMaterias(){
		String xsql="select materias.*, dicta.periodo,dicta.paralelo,dicta.gestion from materias"
+ " inner join dicta on materias.sigla=dicta.sigla; ";				
		return this.jdbcTemplate.queryForList(xsql);
	}
	public List<Map<String,Object>> listarMenu(){
		String xsql="select * "
		+"  from menus "
		+" ";				
		return this.jdbcTemplate.queryForList(xsql);
	}
	public List<Map<String,Object>> listarRoles(){
		String xsql="select * "
		+"  from roles "
		+" ";				
		return this.jdbcTemplate.queryForList(xsql);
	}
	public List<Map<String,Object>> listarRolesMenu(){
		String xsql="select * "
		+"  from rolme "
		+" ";				
		return this.jdbcTemplate.queryForList(xsql);
	}
	public Usuarios getIdusuCi(String ci){
		BeanPropertyRowMapper bprm = new BeanPropertyRowMapper(Usuarios.class);
		String xsql="select *"
		+"  from usuarios "
		+" where cedula=?";
		Usuarios aux=null;
		try {
			aux= (Usuarios) this.jdbcTemplate.queryForObject(xsql,new Object[] {ci},bprm);
		}
		catch(Exception e)
		{
			
			aux=null;
		}
		return aux;
	}
	
	public List<Map<String,Object>> listarUsuariosDatos(){
		String xsql="select a.*,b.login,b.password "
		+"  from usuarios a,datos b"
		+" where a.idusu=b.idusu";				
		return this.jdbcTemplate.queryForList(xsql);
	}
	
	public List<Map<String,Object>> listarUsuariosInscripcion(){
		String xsql="select usuarios.idusu, usuarios.nombre, usuarios.apellido1, usuarios.apellido2, universitarios.ru from usuarios "
				+ "inner join universitarios on usuarios.idusu=universitarios.idusu "
				+ "where usuarios.activo=true";				
		return this.jdbcTemplate.queryForList(xsql);
	}

	public List<Map<String,Object>> listarRolesUsuario(int IDUSU){
		String xsql="select * from roles "
				+ "where roles.idrol not in( "
				+ "SELECT a.idrol "
				+ "FROM usurol a, roles b, usuarios c "
				+ "WHERE a.idrol = b.idrol and a.idusu=c.idusu and a.idusu = ?"
				+ ")";	
		return this.jdbcTemplate.queryForList(xsql,new Object[] {IDUSU});
	}

	public List<Map<String,Object>> listaRolesUsuarioAsignados(int IDUSU){
		String xsql="SELECT b.* " + 
				"FROM usurol a, roles b, usuarios c " + 
				"WHERE a.idrol = b.idrol and a.idusu=c.idusu and a.idusu = ?";
		return this.jdbcTemplate.queryForList(xsql,new Object[] {IDUSU});
	}
	
	
	public int eliminarRolesUsuario(int idusu,int idrol){
		String xsql="delete from usurol where usurol.idusu = ? and usurol.idrol = ?";
		return this.jdbcTemplate.update(xsql, new Object[] {idusu,idrol});
	}
	
	public int agregarRolesUsuario(int idusu,int idrol){
		String xsql="insert into usurol(idusu,idrol) values(?,?)";
		return this.jdbcTemplate.update(xsql, new Object[] {idusu,idrol});
	}
	
 
}


