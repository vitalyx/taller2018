
package controladores;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import model.domain.Datos;
import model.domain.Usuarios;
import model.manager.DatosManager;
import utils.ClaseDESBase64;


		@Controller
		public class MateriasControlador {
			
			@Autowired
			DatosManager datosManager=new DatosManager();
			
			 

			
			
			@RequestMapping({"gestionMaterias.html"})
			public String gestionMateria(Model model,HttpServletRequest req)  throws IOException  {
				List<?> listaMat = this.datosManager.listarMaterias();
				model.addAttribute("xlistaMat", listaMat );
				List<?> listaCarr = this.datosManager.listarCarreras();
				model.addAttribute("xlistaCarr", listaCarr );
				return "itemmat";
			}
			
			
			 
		}