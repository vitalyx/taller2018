package controladores;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import model.domain.Datos;
import model.domain.Usuarios;
import model.manager.DatosManager;
import utils.ClaseDESBase64;


@Controller
public class MenusControlador {
	
	@Autowired
	DatosManager datosManager=new DatosManager();
	
	 
	@RequestMapping({"menu.html"})
	public String vista3(Model model,HttpServletRequest req, HttpServletResponse res)  throws IOException  {
		HttpSession sesion= req.getSession(true);
		String zlogin=(String) sesion.getAttribute("xlogin");
		if (zlogin==null){
			model.addAttribute("xtexto", "Para usar esta opción necesita INICIAR SESIÓN" );
			return "mensajes";
		}else{
			Datos dato = this.datosManager.validarLogin(zlogin.toLowerCase());
			Usuarios usu = this.datosManager.extraerDatos(dato.idusu);
			//List<?> listarol = this.datosManager.listarRoles();
			//model.addAttribute("xlistarol", listarol );			
			//List<?> listasubmenu = this.datosManager.listarSubmenu();
			//model.addAttribute("xlistasubmenu", listasubmenu );
			//List<?> listasroles = this.datosManager.listarRoles();
			//model.addAttribute("xlistasroles", listasroles );
			List<?> listasrolesusu = this.datosManager.listarRolesUsu(usu.idusu);
			model.addAttribute("xlistasroles", listasrolesusu );
			model.addAttribute("xnombre", usu.nombre );
			model.addAttribute("xap", usu.apellido1 );
			model.addAttribute("xam", usu.apellido2 );
			return "menu";
		}
	}
	
	@RequestMapping({"listamenus.html"})
	public String listamenus(Model model,HttpServletRequest req)  throws IOException  {
		
		String xidrol=req.getParameter("xrol");
		
		//System.out.println("llego tambien:"+xidrol);
		
		List<?> listamenu = this.datosManager.listarMenus(Integer.parseInt(xidrol));
		model.addAttribute("xlistamenu", listamenu );	

		List<?> listasubmenu = this.datosManager.listarSubmenu();
		model.addAttribute("xlistasubmenu", listasubmenu );			
		
		return "lismenu";
	}
	
	@RequestMapping({"gestionMenus.html"})
	public String gestionMenus(Model model,HttpServletRequest req)  throws IOException  {
		List<?> listaMen = this.datosManager.listarMenu();
		model.addAttribute("xlistaMen", listaMen );
		List<?> listasubmenu = this.datosManager.listarSubmenu();
		model.addAttribute("xlistasubmenu", listasubmenu );
		return "itemmenu";
	}
/*
	@RequestMapping({"gestionRoles.html"})
	public String gestionRoles(Model model,HttpServletRequest req)  throws IOException  {
		List<?> listasrolesusu = this.datosManager.listarRolesUsu(usu.idusu);
			model.addAttribute("xlistaroles", listasrolesusu );
			model.addAttribute("xnombre", usu.nombre );
		return "itemusu";
	}
*/
	@GetMapping({"modalModificarRoles.html"})
	public String modalModificarRoles(Model model,HttpServletRequest req)  throws IOException  {
		int idusu = Integer.parseInt( req.getParameter("idusu") ) ;
		List<?> listaRolesUsuario = this.datosManager.listarRolesUsuario(idusu);
		List<?> listaRolesUsuarioAsignados = this.datosManager.listaRolesUsuarioAsignados(idusu);
		model.addAttribute("listaRolesUsuario", listaRolesUsuario );
		model.addAttribute("listaRolesUsuarioAsignados", listaRolesUsuarioAsignados );
		model.addAttribute("idusu", idusu );
		return "modalModificarRoles";
	}
	
	@PostMapping({"modalModificarRoles.html"})
	public String postModalModificarRoles(Model model,HttpServletRequest req)  throws IOException  {
		int idusu = Integer.parseInt( req.getParameter("idusu") ) ;
		int idrol = Integer.parseInt( req.getParameter("idrol") ) ;
		String tipo = req.getParameter("tipo") ;

		System.out.println(idusu);
		System.out.println(idrol);
		System.out.println(tipo);
		
		try {
			if(tipo.equals("eliminar"))
				this.datosManager.eliminarRolesUsuario(idusu,idrol);
			else
				this.datosManager.agregarRolesUsuario(idusu,idrol);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		List<?> listaRolesUsuario          = this.datosManager.listarRolesUsuario(idusu);
		List<?> listaRolesUsuarioAsignados = this.datosManager.listaRolesUsuarioAsignados(idusu);
		model.addAttribute("listaRolesUsuario", listaRolesUsuario );
		model.addAttribute("listaRolesUsuarioAsignados", listaRolesUsuarioAsignados );
		model.addAttribute("idusu", idusu );
		return "modalModificarRoles";
	}
	
	
}
	 


