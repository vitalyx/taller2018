package controladores;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import model.domain.Usuarios;
import model.manager.DatosManager;


@Controller
public class InscripcionControlador {
	
	@Autowired
	DatosManager datosManager=new DatosManager();
	
	
	
	
	
	
	@RequestMapping({"listarUsuariosInscripcion.html"})
	public String listarUsuariosInscripcion(Model model,HttpServletRequest req)  throws IOException  {
		
		 
		List<?> listaUsuariosInscripcion = this.datosManager.listarUsuariosInscripcion();
		model.addAttribute("listaUsuariosInscripcion", listaUsuariosInscripcion );	

	 		
		
		return "listaUsuariosInscripcion";
	}

	@RequestMapping({"registrarInscripcion.html"})
	public String registrarInscripcion(Model model,HttpServletRequest req)  throws IOException  {
		System.out.println("registrarInscripcion");
		int idusu=Integer.parseInt(req.getParameter("idusu"));
		int idcarr=Integer.parseInt(req.getParameter("idcarr"));
		String sigla=req.getParameter("sigla");
		String gestion=req.getParameter("gestion");
		String paralelo=req.getParameter("paralelo");
		String periodo=req.getParameter("periodo");
		this.datosManager.registrarInscripcion( idusu, idcarr, sigla, gestion, paralelo, periodo);	
		return "ok";
	}
	@RequestMapping({"deleteInscripcion.html"})
	public String deleteInscripcion(Model model,HttpServletRequest req)  throws IOException  {
		int idusu=Integer.parseInt(req.getParameter("idusu"));
		int idcarr=Integer.parseInt(req.getParameter("idcarr"));
		String sigla=req.getParameter("sigla");
		String gestion=req.getParameter("gestion");
		String paralelo=req.getParameter("paralelo");
		String periodo=req.getParameter("periodo");
		this.datosManager.deleteInscripcion( idusu, idcarr, sigla, gestion, paralelo, periodo);	
		return "ok";
	}

	//@RequestMapping(value = "inscribirAlumno.html", method = RequestMethod.GET, produces = "application/json")
 
	/*
	 
	@RequestMapping(value="/listarAlumnos.html",  method=RequestMethod.GET)
 
	public @ResponseBody Map<String,Object>  listarAlumnos( )    {
		/ *
		List<?> listaMat = this.datosManager.listarMaterias();
		model.addAttribute("xlistaMat", listaMat );
		List<?> listaCarr = this.datosManager.listarCarreras();
		model.addAttribute("xlistaCarr", listaCarr );
		* /
		 
		/ *
		"
		select usuarios.idusu, usuarios.nombre, usuarios.apellido1, usuarios.apellido2 from usuarios
		inner join universitarios on usuarios.idusu=universitarios.idusu
		where usuarios.activo=true"
		* /
		
		 Map<String, Object> rtn = new LinkedHashMap<String, Object>();
	    rtn.put("pic", "sa");
	    rtn.put("potato", "King Potato");

	    return rtn;
	}
	*/
	 
}