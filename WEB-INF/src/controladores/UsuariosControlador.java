package controladores;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import model.domain.Datos;
import model.domain.Usuarios;
import model.manager.DatosManager;
import utils.ClaseDESBase64;


		@Controller
		public class UsuariosControlador {
			
			@Autowired
			DatosManager datosManager=new DatosManager();
			
			
			@RequestMapping({"moddat.html"})
			public void moddat(Model model,HttpServletRequest req, HttpServletResponse res)  throws IOException  {
				//nombre,apellido1,apellido2,sexo,f_nac,cedula,telefono,direccion,foto
				String zidusu=req.getParameter("d_xidusu");
				String zlogin=req.getParameter("d_xlogin");
				String zpassword=req.getParameter("d_xpassword");
				int xres = this.datosManager. modDatos(Integer.parseInt(zidusu),zlogin,zpassword);
				res.sendRedirect("menu.html");
			}
			
			
			@RequestMapping({"valida.html"})
			public void valida(Model model,HttpServletRequest req, HttpServletResponse res)  throws IOException, ParseException  {
				String zlogin=req.getParameter("xlogin");
				String zpassword=req.getParameter("xpassword");
				Datos alu = this.datosManager.validarLogin(zlogin.toLowerCase());
				String xerror="";
				if(alu==null)
				{
					xerror="El usuario no existe";
					res.sendRedirect("index.html"+"?xerror="+xerror);
				}
				else
				{
					if(alu.password.equals(zpassword))
					{
						HttpSession session=req.getSession(true);
						session.setAttribute("xlogin", alu.login);
						res.sendRedirect("menu.html");
					}
					else
					{
						xerror="El password es incorrecto";
						res.sendRedirect("index.html"+"?xerror="+xerror);
					}
				}
			}
			
			@RequestMapping({"gestionUsuario.html"})
			public String gestionUsuario(Model model,HttpServletRequest req)  throws IOException  {
				List<?> listausudat = this.datosManager.listarUsuariosDatos();
				model.addAttribute("xlistausu", listausudat );
				return "itemusu";
			}
			
			@RequestMapping({"gestionRoles.html"})
			public String gestionRoles(Model model,HttpServletRequest req)  throws IOException  {
				List<?> listaRol = this.datosManager.listarRoles();
				model.addAttribute("xlistaRol", listaRol );
				List<?> listaMen = this.datosManager.listarMenu();
				model.addAttribute("xlistaMen", listaMen );
				List<?> listaRolme = this.datosManager.listarRolesMenu();
				model.addAttribute("xlistaRolme", listaRolme );
				return "itemrol";
			}
			
			@RequestMapping({"desconectar.html"})
			public String desconectar(Model model,HttpServletRequest req,HttpServletResponse res)  throws IOException  {
				HttpSession sesion=req.getSession(true);
				sesion.removeAttribute("xlogin");		
				model.addAttribute("xtexto", "Desconectado" );
				return "mensajes";
			}
			
			@RequestMapping({"addusu.html"})
			public void addlumno2(Model model,HttpServletRequest req, HttpServletResponse res)  throws IOException  {
				String znombre=req.getParameter("xnombre");
				String zapellido1=req.getParameter("xapellido1");
				String zapellido2=req.getParameter("xapellido2");
				String zsexo=req.getParameter("xsexo");
				String zf_nac=req.getParameter("xf_nac");
				Date xfecha=new Date();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				try{
					xfecha=format.parse(zf_nac);
				}catch (ParseException e){
					e.printStackTrace();
				}
				String zcedula=req.getParameter("xcedula");
				String ztelefono=req.getParameter("xtelefono");
				String zdireccion=req.getParameter("xdireccion");
				String zfoto=req.getParameter("xfoto");
				if (zfoto==""){
					zfoto="#";
				}
				String zlogin=req.getParameter("xlogin");
				int xres = this.datosManager. insertUsuarios(znombre,zapellido1,zapellido2,zsexo,xfecha,zcedula,ztelefono,zdireccion, zlogin);
				Usuarios usu = this.datosManager.getIdusuCi(zcedula);
				xres = this.datosManager. insertDatos(usu.idusu,zlogin,zcedula);
				res.sendRedirect("menu.html");
			}
			
			@RequestMapping({"modusu.html"})
			public void addlumno3(Model model,HttpServletRequest req, HttpServletResponse res)  throws IOException  {
				//nombre,apellido1,apellido2,sexo,f_nac,cedula,telefono,direccion,foto
				String zidusu=req.getParameter("m_xidusu");
				System.out.println(zidusu);
				String znombre=req.getParameter("m_xnombre");
				System.out.println(znombre);
				String zapellido1=req.getParameter("m_xapellido1");
				System.out.println(zapellido1);
				String zapellido2=req.getParameter("m_xapellido2");
				System.out.println(zapellido2);
				String zsexo=req.getParameter("m_xsexo");
				System.out.println(zsexo);
				String zf_nac=req.getParameter("m_xf_nac");
				Date xfecha=new Date();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				try {
					xfecha=format.parse(zf_nac);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				System.out.println(xfecha);
				String zcedula=req.getParameter("m_xcedula");
				System.out.println(zcedula);
				String ztelefono=req.getParameter("m_xtelefono");
				System.out.println(ztelefono);
				String zdireccion=req.getParameter("m_xdireccion");
				System.out.println(zdireccion);
				String zfoto="#";
				String zlogin=req.getParameter("m_xlogin");
				int xres = this.datosManager. modUsuarios(Integer.parseInt(zidusu),znombre,zapellido1,zapellido2,zsexo,xfecha,zcedula,ztelefono,zdireccion,zfoto);
				xres = this.datosManager. modDatosLogin(Integer.parseInt(zidusu),zlogin);
				res.sendRedirect("menu.html");
			}
			
			
			@RequestMapping({"delAlu.html"})
			public void delalu(Model model,HttpServletRequest req, HttpServletResponse res)  throws IOException  {
				//se recepciona los datos que viene INICIO.VM
				String zidusu=req.getParameter("xidusu");
				int xres = this.datosManager.delUsuarios(Integer.parseInt(zidusu));
				res.sendRedirect("menu.html");
			}
			@RequestMapping({"habAlu.html"})
			public void habalu(Model model,HttpServletRequest req, HttpServletResponse res)  throws IOException  {
				//se recepciona los datos que viene INICIO.VM
				String zidusu=req.getParameter("xidusu");
				int xres = this.datosManager.habUsuarios(Integer.parseInt(zidusu));
				res.sendRedirect("menu.html");
			}
			 
			 
		}
			