 
package controladores;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import model.domain.Datos;
import model.domain.Usuarios;
import model.manager.DatosManager;
import utils.ClaseDESBase64;


		@Controller
		public class CarrerasControlador {
			
			@Autowired
			DatosManager datosManager=new DatosManager();
			
			@RequestMapping({"gestionCarrera.html"})
			public String gestionCarrera(Model model,HttpServletRequest req)  throws IOException  {
				List<?> listaCarr = this.datosManager.listarCarreras();
				model.addAttribute("xlistaCarr", listaCarr );
				return "itemcarr";
			}
			
			@RequestMapping({"addcarr.html"})
			public void addCarr(Model model,HttpServletRequest req, HttpServletResponse res)  throws IOException  {
				//nombre,apellido1,apellido2,sexo,f_nac,cedula,telefono,direccion,foto
				String znombre=req.getParameter("xnombre");
				String zdireccion=req.getParameter("xdireccion");
				String ztelefono=req.getParameter("xtelefono");
				int xres = this.datosManager. insertCarreras(znombre,zdireccion,ztelefono);
				res.sendRedirect("menu.html");
			}
			@RequestMapping({"modcarr.html"})
			public void modCarr(Model model,HttpServletRequest req, HttpServletResponse res)  throws IOException  {
				String zidcarr=req.getParameter("m_xidcarr");
				String znombre=req.getParameter("m_xnombre");
				String zdireccion=req.getParameter("m_xdireccion");
				String ztelefono=req.getParameter("m_xtelefono");
				int xres = this.datosManager. modCarreras(Integer.parseInt(zidcarr),znombre,zdireccion,ztelefono);
				res.sendRedirect("menu.html");
			}
			@RequestMapping({"delCarr.html"})
			public void delCarr(Model model,HttpServletRequest req, HttpServletResponse res)  throws IOException  {
				//se recepciona los datos que viene INICIO.VM
				String zidcarr=req.getParameter("xidcarr");
				System.out.println(zidcarr);
				int xres = this.datosManager.delCarreras(Integer.parseInt(zidcarr));
				res.sendRedirect("menu.html");
			}
			@RequestMapping({"habCarr.html"})
			public void habCarr(Model model,HttpServletRequest req, HttpServletResponse res)  throws IOException  {
				//se recepciona los datos que viene INICIO.VM
				String zidcarr=req.getParameter("xidcarr");
				System.out.println(zidcarr);
				int xres = this.datosManager.habCarreras(Integer.parseInt(zidcarr));
				res.sendRedirect("menu.html");
			}
			
			
			
			
		}